<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //Colunas do bando de dados adicionando comentário
    protected $fillable = ['nome', 'telefone', 'nascimento', 'idade'];
}
